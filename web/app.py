from flask import Flask,render_template, request

app = Flask(__name__)

@app.route("/<path:name>")

def hello(name):
    
#    print(name)
    if name.endswith(".html") or name.endswith(".css"):
        #if path name end with .html or .css
        try:
            if ".." in name or "~" in name or "//" in name:
                #if path contain these character
                return status_forbidden(403)
            #return function 403
            else:
                return render_template(name)
        except:
           
            return status_not_found(404)

@app.errorhandler(404)# function that if return 403 then jump self design 403 webpage
def status_not_found(error):
    return render_template("STATUS_NOT_FOUND.html"),404

@app.errorhandler(403)#function that if return 404 then jump self design 404 webpage
def status_forbidden(error):
    return render_template("STATUS_FORBIDDEN.html"),403

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
